// DELETE /videos/:id

import client from "../../lib/client.js";

export default async ({ params, response }) => {
  if (!params.id) {
    res.send("Mising param :id");
    res.end();
  }

  try {
    client.query(` DELETE FROM videos WHERE videos.id = ${params.id}`);

    response.body = "Success";
  } catch (error) {
    response.body = { error: JSON.stringify(error) };
    response.stats = 500;
  }
};
