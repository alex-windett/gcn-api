import { getQuery } from "https://deno.land/x/oak/helpers.ts";

import client from "../../lib/client.js";

export default async (ctx) => {
  const { response } = ctx;

  try {
    const qry = getQuery(ctx, { mergeParams: true });

    let sql = "";

    // If there is a query string, build sql query, otherwise return all
    if (!!qry.title) {
      let like = qry.title.split("|");
      like = like.map((l) => `lower(videos.title) like lower('%${l}%')`);
      like = like.join(" or ");

      sql = `SELECT videos.title, videos.id from videos WHERE ${like}`;
    } else {
      sql = `SELECT * from videos`;
    }

    const { rows: items } = await client.execute(sql);

    response.status = 200;
    response.body = {
      count: items.length,
      items,
    };
  } catch (error) {
    console.log(error);
    response.status = 500;
    response.body = { error: JSON.stringify(error) };
  }
};
