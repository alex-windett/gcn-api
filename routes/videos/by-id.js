// GET /videos/:id

import client from "../../lib/client.js";

export default async ({ response, params }) => {
  if (!params.id) {
    res.send("Missing param :id");
    res.end();
  }

  try {
    const items = await client.query(
      ` SELECT videos.id, videos.title, videos.date FROM videos WHERE videos.id = ${params.id} `
    );

    response.body = {
      count: items.length,
      items,
    };
  } catch (error) {
    console.log(error);
    response.body = { error: JSON.stringify(error) };
    response.status(500);
  }
};
