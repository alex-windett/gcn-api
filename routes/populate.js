import { config } from "https://deno.land/x/dotenv/mod.ts";

import client from "../lib/client.js";

const env = config();

// Hacky - apologies
const time = (time) => time.replace("Z", "").replace("T", " ");

const channels = [
  {
    name: "GlobalCyclingNetwork",
    id: "UCuTaETsuCOkJ0H_GAztWt0Q",
  },
  {
    name: "globalmtb",
    id: "UC_A--fhX5gea0i4UtpD99Gg",
  },
];

const filters = async () => {
  const data = await Deno.readTextFile("./static/filters.txt");
  return data.split("\n").join("|");
};

export default async ({ response }) => {
  try {
    const q = await filters();

    const setQuery = ({ channelId, pageToken }) => {
      const queries = {
        channelId,
        q,
        part: "snippet",
        maxResults: env.RESULTS_PER_CHANNEL || 10,
        key: env.YOUTUBE_KEY,
      };

      // Convert object to query string
      const query = Object.keys(queries)
        .reduce((acc, curr) => {
          return [...acc, `${curr}=${queries[curr]}`];
        }, [])
        .join("&");

      return "https://www.googleapis.com/youtube/v3/search?" + query;
    };

    /** 
     * Recursivley call the api for paginated results
     * 
   
    let items = [];

    const getData = async (pageToken = "") => {
      const url = setQuery({ pageToken, channelId: "UCuTaETsuCOkJ0H_GAztWt0Q" });

      const { data } = await axios({
        method: "get",
        url,
        headers: {
          Accept: "application/json",
        },
      });

      data.items.forEach((video) => {
        items = [...items, { title: video.snippet.title, time: video.publishTime }];
      });

      if (data.nextPageToken) {
        await getData(data.nextPageToken);
      }

      return items;
    };

    const rows = await getData();
    */

    let results = await Promise.all(
      channels.map(async (channel) => {
        await client.execute(` INSERT INTO channels( channel_name ) VALUES ( '${channel.name}'  )`);

        const url = setQuery({ channelId: channel.id });

        const data = await fetch(url);
        const json = await data.json();

        return json.items;
      })
    );

    // Flat Map results, and return formatted sql query variables
    results = results.flatMap((x) => x);
    const videoVal = results
      .map((v) => `( '${v.snippet.title}', '${time(v.snippet.publishedAt)}' )`)
      .join(",");

    await client.execute(
      `INSERT INTO videos(
      title,
      date
    ) 
    VALUES ${videoVal}`
    );

    const items = results.map((r) => ({ title: r.snippet.title, date: r.snippet.publishedAt }));

    response.status = 200;
    response.body = {
      query: q,
      count: results.length,
      items,
    };
  } catch (error) {
    console.log(error);
    response.status = 500;
    response.body = { error: JSON.stringify(error) };
  }
};
