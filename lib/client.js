import { Client } from "https://deno.land/x/mysql/mod.ts";
import { config } from "https://deno.land/x/dotenv/mod.ts";
const env = config();

const { DB_PASSWORD, DB_NAME, DB_USER, DB_HOST } = env;

export default await new Client().connect({
  hostname: "127.0.0.1",
  username: DB_USER,
  password: DB_PASSWORD,
  db: DB_NAME,
});
