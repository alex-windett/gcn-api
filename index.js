import { Application, Router } from "https://deno.land/x/oak/mod.ts";
import client from "./lib/client.js";

import home from "./routes/index.js";
import populate from "./routes/populate.js";
import allVideos from "./routes/videos/index.js";
import deleteVideo from "./routes/videos/delete.js";
import videoById from "./routes/videos/by-id.js";

const app = new Application();
const router = new Router();
app.use(router.allowedMethods());

await client.execute("USE mydb");

router
  .get("/", home)

  .get("/populate", populate)
  .get("/videos", allVideos)
  .get("/videos/:id", videoById)
  .delete("/videos/:id", deleteVideo);

app.use(router.routes());

console.log("Running at http://localhost:3000");

await app.listen({ port: 3000 });
