# GCN Deno API

This API uses [Deno](https://deno.land), I originally created an Express version which can be found on the [node branch](https://gitlab.com/alex-windett/gcn-api/-/tree/node). I had a bit of spare time so converted it over to Deno. If there are any install issues with Deno, the Node version might be more compatible.

## Install

Install [Deno](https://deno.land/manual/getting_started/installation) via:

**Brew**

```
brew install deno
```

**Shell**

```
curl -fsSL https://deno.land/x/install/install.sh | sh
```

## Database

Create your SQL databse by runing [this query](./static/youtube.sql).

I altered the ID columns to add AUTO_INCREMENT. Everything else was as the original.

## Env Variables

Create and `.env`file, or copy and paste [the example](.env.example), and populate with the following values:

```
DB_PASSWORD
DB_NAME
DB_USER
YOUTUBE_KEY

# Use 127.0.0.1 instead of localhost
DB_HOST

# (OPTIONAL) Defaults to 5 if unset
RESULTS_PER_CHANNEL
```

## Serve API

### Dev

This has a watch and live reload.

`./dev.sh`

### Prod

`./prod.sh`

## API Routes

- [Populate](./readme/populate.md)
  - `/populate`
- [Get Videos](./readme/videos.md)
  - `/videos`
  - `/videos?title=[string]`
- [Get Video by ID](./readme/videos-by-id.md)
  - `/videos/:id`
- [Delete Video](./readme/delete.md)
  - `/videos/:id`

## YouTube filters

These are stored in [./static/filters.txt](./static/filters.txt) and are read by strings on a seperate lines. The server will need to be restarted if these are updated.
