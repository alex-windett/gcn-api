# Get All Videos

Retrive all videos

**URL** : `/videos`

**Method** : `GET`

**Query Params** :

- `title` - search for video with title like string. Search for many titles by seperating searchs with `|`. This is not case sensitive

**Example** :

- `/videos`
- `/videos?title=race report`
- `/videos?title=race report|gmbn`

## Success Response

**Code** : `200 OK`

```json
{
  "count": integer,
  "items": [Video],
}

/* VIDEO */
{
  "title": string,
  "date": dateTime,
}
```

## Error Response

**Code** : `500`

```json
{
  "error": string
}
```
