# Populate Database

Queries the GCN and GMBN YouTube channels via the YouTube API and populates database with results.

**URL** : `/populate`

**Method** : `GET`

## Success Response

**Code** : `200 OK`

```json
{
  "query": string,
  "count": integer,
  "items": [Video],
}

/* VIDEO */
{
  "title": string,
  "date": dateTime,
}
```

## Error Response

**Code** : `500`

```json
{
  "error": string
}
```
