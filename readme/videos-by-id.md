# Get All Videos

Retrive all videos

**URL** : `/videos/:id`

**Method** : `GET`

**params** :

- `id` - Video ID

**Example** : `/videos/2`

## Success Response

**Code** : `200 OK`

```json
{
  "count": integer,
  "items": [Video],
}

/* VIDEO */
{
  "title": string,
  "date": dateTime,
}
```

## Error Response

**Code** : `500`

```json
{
  "error": string
}
```
