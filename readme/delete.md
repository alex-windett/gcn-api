# Delete Video

Delete video by ID

**URL** : `/videos/:id`

**Method** : `DELETE`

**params** :

- `id` - Video ID

**Example** : `/videos/2`

## Success Response

**Code** : `200 OK`

```json
OK
```

## Error Response

**Code** : `500`

```json
{
  "error": string
}
```
